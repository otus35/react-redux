import { Action, AnyAction, ThunkAction, ThunkDispatch } from "@reduxjs/toolkit";
import { store } from "../../store";

export type TRootState = ReturnType<typeof store.getState>;

export type TAppDispatch = typeof store.dispatch & ThunkDispatch<TRootState, null, AnyAction>;

export type TAppThunk = ThunkAction<void, TRootState, null, Action<string>>;

export type TAppState = {
    pageTitle: string;
};