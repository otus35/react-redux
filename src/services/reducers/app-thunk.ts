
import { setPageTitle } from "./app-slice";
import { TAppDispatch, TAppThunk } from "./types";

export const SetPageTitleThunk = (pageTitle: string): TAppThunk => {
    return async (dispatch: TAppDispatch) => {
        dispatch(setPageTitle(pageTitle));
    };
}