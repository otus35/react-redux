import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TAppState } from './types';

const appInitialState: TAppState = {
    pageTitle: ''
};

const appSlice = createSlice({
    name: 'APP',
    initialState: appInitialState,
    reducers: {
        setPageTitle: (state: TAppState, action: PayloadAction<string>) => {
            state.pageTitle = action.payload;;
        }
    }
})
export const { setPageTitle } = appSlice.actions;
export const appReducer = appSlice.reducer;