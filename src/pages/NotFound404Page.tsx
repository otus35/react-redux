import { Link } from "react-router-dom";

function NotFound404Page() {
  return (
    <div>
      <h1>Оуч! 404 Error</h1>
      <p>Такой страницы не существует</p>
      <p>Проверьте адрес или перейдите на <Link className="text text_color_accent" to='/'>главную страницу</Link></p>
    </div>
  );
}

export default NotFound404Page;
