import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch } from "../hooks/use-app-dispatch";
import { SetPageTitleThunk } from "../services/reducers/app-thunk";

function LoginPage() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState("");

  useEffect(() => {
    dispatch(SetPageTitleThunk("Логин"));
  }, []);

  const handleSubmit = async (e: any) => {
    setStatus("Отправка запроса");
    e.preventDefault();
    await fetch('/post', { method: 'post', body: JSON.stringify({ username, password }) })
      .then((data: any) => {
        console.log(data);
        if (data.ok) {
          setStatus("Запрос успешно выполнен");
          navigate("/");
        }
        else {
          console.log(data);
          if (data.statusText) {
            setStatus("Запрос выполнился с ошибкой: " + data.statusText);
          }
          else {
            setStatus("Json API вернул success != true" + JSON.stringify(data));
          }
          return data;
        }
      });
  };
  return (
    <form onSubmit={handleSubmit} className="main">
      <h1>Станица авторизации</h1>
      <label>Логин или Email</label>
      <input
        name="login"
        autoComplete="false"
        className="form-control"
        type="text"
        value={username}
        onChange={(e) => setUserName(e.target.value)}
      />
      <label>Пароль</label>
      <input
        name="password"
        className="form-control"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button type='submit'>Войти</Button>
      {status && <div>{status}</div>}
      <Link to='/registrer'>Создать нового пользователя</Link>
    </form>
  );
}

export default LoginPage;
