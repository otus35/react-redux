import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch } from '../hooks/use-app-dispatch';
import logo from '../logo.svg';
import { SetPageTitleThunk } from '../services/reducers/app-thunk';

function HomePage() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(SetPageTitleThunk("Главная страница"));
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <Link to='/login'>Уже есть логин?</Link>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default HomePage;

