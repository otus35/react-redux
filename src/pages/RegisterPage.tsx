import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useAppDispatch } from "../hooks/use-app-dispatch";
import { SetPageTitleThunk } from "../services/reducers/app-thunk";

function RegisterPage() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(SetPageTitleThunk("Регистрация"));
  }, []);

  return (
    <div>
      <h1>Создать пользователя</h1>
      <Link to='/login'>Уже есть логин?</Link>
    </div>
  );
}

export default RegisterPage;
