import { configureStore } from '@reduxjs/toolkit'
import thunkMiddleware from 'redux-thunk';
import { appReducer } from './services/reducers/app-slice';

export const store = configureStore({
    reducer: { app: appReducer },
    middleware: [thunkMiddleware],
    devTools: process.env.NODE_ENV !== 'production',
})