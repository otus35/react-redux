import { useDispatch } from "react-redux";
import { TAppDispatch } from "../services/reducers/types";

export const useAppDispatch = () => useDispatch<TAppDispatch>();