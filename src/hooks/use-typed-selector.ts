import { TypedUseSelectorHook, useSelector } from "react-redux";
import { TRootState } from "../services/reducers/types";

export const useTypedSelector: TypedUseSelectorHook<TRootState> = useSelector;