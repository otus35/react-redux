import { Route, Routes } from 'react-router-dom';
import NotFound404Page from '../pages/NotFound404Page';
import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import Register from '../pages/RegisterPage';
import '../styles.css';
import { useTypedSelector } from '../hooks/use-typed-selector';
import { useEffect } from 'react';
import withTime from './hoc';

function App() {
  const { pageTitle } = useTypedSelector(state => state.app);

  useEffect(() => {
    document.title = pageTitle;
  }, [pageTitle]);
  const WithTimeHomePage = withTime(HomePage);
  const WithTimeLoginPage = withTime(LoginPage);
  const WithTimeRegister = withTime(Register);
  return (
    <div className="App-header">
      <Routes>
        <Route path='/' element={<WithTimeHomePage />} />
        <Route path='login' element={<WithTimeLoginPage />} />
        <Route path='registrer' element={<WithTimeRegister />} />
        <Route path='*' element={<NotFound404Page />} />
      </Routes>
    </div>
  );
}

export default App;
