import { useTypedSelector } from "../hooks/use-typed-selector";

function withTime(Component: any) {
    return (props: any) => {
        const { pageTitle } = useTypedSelector(state => state.app);
        return (
            <div>
                <Component {...props} />
                <p className="d_time">{pageTitle}</p>
            </div>);
    }
}
export default withTime;